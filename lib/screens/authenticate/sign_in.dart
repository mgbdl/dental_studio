import 'package:flutter/material.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {

  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Color(0xFF18D191)),
          title: Text(
            'Iniciar sesion',
            style: TextStyle(
                color: Colors.blue,
                fontSize: 30.0
            ),
          ),
        ),
        body: SafeArea(
            child: Center(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 50.0,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Form(
                                child: Column(
                                  children: <Widget>[
                                    SizedBox(height: 20.0),
                                    TextFormField(
                                      onChanged:(val) {
                                        print('Siguiente');
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'Correo electrónico',
                                        fillColor: Colors.white,
                                        filled: true,
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(25.0),
                                          borderSide: BorderSide(color: Colors.white),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(25.0),
                                          borderSide: BorderSide(color: Colors.white),
                                        ),
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                    SizedBox(height: 20.0),
                                    TextFormField(
                                      obscureText: hidePassword,
                                      onChanged:(val) {
                                        print('Siguiente');
                                      },
                                      decoration: InputDecoration(
                                        hintText: 'Contraseña',
                                        fillColor: Colors.white,
                                        filled: true,
                                        suffixIcon: IconButton(
                                          icon: Icon(Icons.remove_red_eye),
                                          onPressed: () {
                                            setState(() => hidePassword = !hidePassword);
                                          },
                                          color: (hidePassword) ? Colors.grey : Colors.blue,
                                        ),
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(25.0),
                                          borderSide: BorderSide(color: Colors.white),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(25.0),
                                          borderSide: BorderSide(color: Colors.white),
                                        ),
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                  ],
                                )
                              )
                            )
                        )
                      ],
                    ),
                    SizedBox(
                      height: 100.0,
                    ),
                    Row(
                      children: <Widget>[
                        Flexible(
                          child: Padding(
                            padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 10.0),
                            child: GestureDetector(
                              onTap: () {
                                print('Siguiente');
                              },
                              child: Container(
                                alignment: Alignment.center,
                                height: 60.0,
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(50.0)
                                ),
                                child: Text(
                                  'INICIO DE SESION',
                                  style: TextStyle(
                                    fontSize: 20.0,
                                    color: Colors.white,
                                  )
                                )
                              ),
                            )
                          )
                        )
                      ]
                    ),
                    SizedBox(height: 35.0),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: Padding(
                          padding: EdgeInsets.only(),
                          child: GestureDetector(
                            onTap: () {
                                print('Tienes problemas');
                            },
                            child: Container(
                              alignment: Alignment.center,
                              child: Text(
                                'Tienes problemas para iniciar sesion? \n'
                                    '             Aqui te podemos ayudar.'
                                )
                              )
                            )
                          )
                        )
                      ]
                    ),
                  ],
                )
            )
        )
    );
  }
}
