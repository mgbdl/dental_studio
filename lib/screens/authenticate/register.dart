import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  bool hidePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: IconThemeData(color: Color(0xFF18D191)),
          title: Text(
            'Crear Cuenta',
            style: TextStyle(
              color: Colors.blue,
              fontSize: 30.0
            ),
          ),
        ),
        body: SafeArea(
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 50.0,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Form(
                            child: Column(
                              children: <Widget>[
                                SizedBox(height: 20.0),
                                TextFormField(
                                  onChanged:(val) {
                                    print('Siguiente');
                                  },
                                  decoration: InputDecoration(
                                    hintText: 'Cual es tu correo electrónico?',
                                    fillColor: Colors.white,
                                    filled: true,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(color: Colors.white),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(color: Colors.white),
                                    ),
                                  ),
                                  keyboardType: TextInputType.emailAddress,
                                ),
                                Align(
                                    alignment: Alignment.centerLeft,
                                    child: Container(
                                        child: Text(
                                          'Tendrás que confirmar esta dirección de correo electrónico.',
                                          textAlign: TextAlign.right,
                                          style: TextStyle(
                                            color: Colors.grey,
                                          ),
                                        )
                                    )
                                ),
                                SizedBox(height: 20.0),
                                TextFormField(
                                  obscureText: hidePassword,
                                  onChanged:(val) {
                                    print('Siguiente');
                                  },
                                  decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                      icon: Icon(Icons.remove_red_eye),
                                      onPressed: () {
                                        setState(() => hidePassword = !hidePassword);
                                      },
                                      color: (hidePassword) ? Colors.grey : Colors.blue,
                                    ),
                                    hintText: 'Elige una contraseña',
                                    fillColor: Colors.white,
                                    filled: true,
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(color: Colors.white),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                      borderSide: BorderSide(color: Colors.white),
                                    ),
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.centerLeft,
                                  child: Container(
                                    child: Text(
                                      'Debe tener al menos ocho caracteres.',
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    )
                                  )
                                )
                              ],
                            )
                          )
                        )
                      )
                    ],
                  ),
                  SizedBox(
                    height:70.0,
                  ),
                  Row(
                    children: <Widget>[
                      Flexible(
                        child: Padding(
                          padding: EdgeInsets.only(left: 50.0, right: 50.0, top: 10.0),
                          child: GestureDetector(
                            onTap: () {
                              print('Siguiente');
                            },
                            child: Container(
                              alignment: Alignment.center,
                              height: 60.0,
                              decoration: BoxDecoration(
                                color: Colors.green,
                                borderRadius: BorderRadius.circular(50.0)
                              ),
                              child: Text(
                                'SIGUIENTE',
                                style: TextStyle(
                                  fontSize: 20.0,
                                  color: Colors.white,
                                )
                              )
                            ),
                          )
                        )
                      )
                    ]
                  ),
                ],
              )
            )
        )
    );
  }
}
